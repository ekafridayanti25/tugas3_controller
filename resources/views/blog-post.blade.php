@extends('layouts.app')
@section('class')
    blog-post
@endsection
@section('content')

<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1>my <span>blog</span></h1>
    <span class="title-bg">posts</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
        <div class="row">
            <!-- Article Starts -->
            <article class="col-12">
                <!-- Meta Starts -->
                <div class="meta open-sans-font">
                    <span><i class="fa fa-user"></i>eka frida</span>
                    <span class="date"><i class="fa fa-calendar"></i> 25 September 2021</span>
                    <span><i class="fa fa-tags"></i> wordpress, technology, digitalisation</span>
                </div>
                <!-- Meta Ends -->
                <!-- Article Content Starts -->
                <h1 class="text-uppercase text-capitalize">More Thoughts On Machine Intteligance</h1>
                <img src="img/blog/blog-post-1.jpg" class="img-fluid" alt="Blog image"/>
                <div class="blog-excerpt open-sans-font pb-5">
                    <p>Wouldn't you say that we are increasingly reliant on technology in our lives> indeed, we do. Just look at the habit that we have adopted
                        from the moment we wake up to the moment we go to bed, in which our first and last interaction of the day is likely to be with a digital gadget than with another human being
                    </p>
                    <p>As a matter of fact, Internet addiction is becaming a real mental healt problem for an increasingly number of people,
                        while those born in the age of the internet, these digital natives, canot imagine a file when they are not connectedto Facebook, Instagram, Youtube, Tiktok, WhatsApp, and the voice of Siri and Alexa.
                    </p>
                    <p>Our mode of communocation, even with our most nearest and dearest, is more and more through video calls, chats, and social media posts.
                        Our meetings with colleagues and clients are through Zoom, Google Meet or WhatsApp Video.
                    </p>
                    <p>What was unthinkable a mere decade age, this ability to follow any person we want anywhere in the world simply by following their social media feeds
                        or by putting a tracker on their phone, is now common place. As a matter of fact, we are surounded with so much technology, anuone, especially the government, can know exactly whar we are doing, where
                        and with whom, not only becaouse we have with us a trackable gadget, but because the entire world is being tranformed into one digital grid from which we cannot escape even if we want to.
                    </p>
                </div>
                <!-- Article Content Ends -->
            </article>
            <!-- Article Ends -->
        </div>
    </div>
</section>
@endsection
