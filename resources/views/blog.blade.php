@extends('layouts.app')
@section('class')
    blog
@endsection
@section('content')
   
<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1>my <span>blog</span></h1>
    <span class="title-bg">posts</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
        <!-- Articles Starts -->
        <div class="row">
            <!-- Article Starts -->
            <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                <article class="post-container">
                    <div class="post-thumb">
                        <a href="/blog-post" class="d-block position-relative overflow-hidden">
                            <img src="img/blog/blog-post-1.jpg" class="img-fluid" alt="Blog Post">
                        </a>
                    </div>
                    <div class="post-content">
                        <div class="entry-header">
                            <h3><a href="/blog-post">More Thoughts On Machine Intelligance</a></h3>
                        </div>
                        <div class="entry-content open-sans-font">
                            <p>Wouldn't you say that we are increasingly reliant on technology in our lives? indeed, we do...
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            <!-- Article Ends -->
            <!-- Article Starts -->
            <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                <article class="post-container">
                    <div class="post-thumb">
                        <a href="/blog-post" class="d-block position-relative overflow-hidden">
                            <img src="img/blog/blog-post-2.jpg" class="img-fluid" alt="">
                        </a>
                    </div>
                    <div class="post-content">
                        <div class="entry-header">
                            <h3><a href="/blog-post">Will Humans Still Be Relevant?</a></h3>
                        </div>
                        <div class="entry-content open-sans-font">
                            <p>My musing on our increasing depedency on AI and Gadget in practically every sphere our lives...
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            <!-- Article Ends -->
            <!-- Article Starts -->
            <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                <article class="post-container">
                    <div class="post-thumb">
                        <a href="/blog-post" class="d-block position-relative overflow-hidden">
                            <img src="img/blog/blog-post-3.jpg" class="img-fluid" alt="">
                        </a>
                    </div>
                    <div class="post-content">
                        <div class="entry-header">
                            <h3><a href="/blog-post">Everything You Need to Know About Web Accessibility</a></h3>
                        </div>
                        <div class="entry-content open-sans-font">
                            <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore...
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            <!-- Article Ends -->
            <!-- Article Starts -->
            <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                <article class="post-container">
                    <div class="post-thumb">
                        <a href="/blog-post" class="d-block position-relative overflow-hidden">
                            <img src="img/blog/blog-post-4.jpg" class="img-fluid" alt="">
                        </a>
                    </div>
                    <div class="post-content">
                        <div class="entry-header">
                            <h3><a href="/blog-post">Your Memory is a Tool</a></h3>
                        </div>
                        <div class="entry-content open-sans-font">
                            <p>When at home alone, whether self-isolating on just because you're afraid of exposing youself to the ...
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            <!-- Article Ends -->
            <!-- Article Starts -->
            <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                <article class="post-container">
                    <div class="post-thumb">
                        <a href="/blog-post" class="d-block position-relative overflow-hidden">
                            <img src="img/blog/blog-post-5.jpg" class="img-fluid" alt="">
                        </a>
                    </div>
                    <div class="post-content">
                        <div class="entry-header">
                            <h3><a href="/blog-post">The True Meaning of Freedom</a></h3>
                        </div>
                        <div class="entry-content open-sans-font">
                            <p>The idea of total freedom surely does not exist. The very fact that we are operating in a three dimensional universe...
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            <!-- Article Ends -->
            <!-- Article Starts -->
            <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-30">
                <article class="post-container">
                    <div class="post-thumb">
                        <a href="/blog-post" class="d-block position-relative overflow-hidden">
                            <img src="img/blog/blog-post-6.jpg" class="img-fluid" alt="">
                        </a>
                    </div>
                    <div class="post-content">
                        <div class="entry-header">
                            <h3><a href="/blog-post">The Search For Meaning</a></h3>
                        </div>
                        <div class="entry-content open-sans-font">
                            <p>We humans always have to look for meaning in our lives. In everything we do and in everuthing we see around us...
                            </p>
                        </div>
                    </div>
                </article>
            </div>
            <!-- Article Ends -->
            <!-- Pagination Starts -->
            <div class="col-12 mt-4">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center mb-0">
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item active"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                    </ul>
                </nav>
            </div>
            <!-- Pagination Ends -->
        </div>
        <!-- Articles Ends -->
    </div>

</section>



@endsection
